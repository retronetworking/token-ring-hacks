#pragma once
#include <stdint.h>
#include <stdbool.h>

#define MAC_TABLE_SIZE	64

struct mac_table {
	const char *name;
	uint8_t addr[MAC_TABLE_SIZE][6];
	unsigned int num_entries;
};

static inline bool mac_table_empty(const struct mac_table *tbl)
{
	return tbl->num_entries == 0;
}

void mac_table_init(struct mac_table *tbl, const char *name);
int mac_table_add(struct mac_table *tbl, const uint8_t *addr);
bool mac_table_contains(struct mac_table *tbl, const uint8_t *addr);
int mac_table_read(struct mac_table *tbl, const char *fname);
