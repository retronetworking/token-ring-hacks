token ring hacks
================

This repository contains some random hacks around Token Ring networks,
developed in the context of retronetworking activities long after Token
Ring became obsolete


tr-bridge
---------

This is a small program that opens AF_PACKET sockets on an ethernet and
a token ring net-device.  This means you need to use a Linux kernel <= 3.5
as token ring support was removed at that point.

The program then bridges 802.2 LLC packets (such as those used in IBM SNA)
between the two interfaces, performing the required bit-swapping of MAC
addresses.

There are two modes of operation: with and without MAC address tables.

The first mode will bridge all frames, while the second will only selectively
bridge frames for certain destination MAC addresses.

This program has shown to work in principle, but there is one known problem:
As it operates in promiscuous mode on the TR side, it does not set the 'C'
flag in the token, meaning that it looks to the sender as if the frame was not
properly received (copied) by the destination mac address.  This may cause
unnecessary retransmissions of frames on TR.

As the setting of this bit is done inside the network device hardware/firmware,
it is currently not known how (if any) work-around to that problem is possible.
