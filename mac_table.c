
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <errno.h>

#include "mac_table.h"
#include "utils.h"


bool mac_table_contains(struct mac_table *tbl, const uint8_t *addr)
{
	unsigned int i;
	for (i = 0; i < tbl->num_entries; i++) {
		if (!memcmp(tbl->addr[i], addr, 6))
			return true;
	}
	return false;
}

int mac_table_add(struct mac_table *tbl, const uint8_t *addr)
{
	if (tbl->num_entries == MAC_TABLE_SIZE-1)
		return -ENOSPC;

	memcpy(tbl->addr[tbl->num_entries++], addr, 6);

	return 0;
}

void mac_table_init(struct mac_table *tbl, const char *name)
{
	memset(tbl, 0, sizeof(*tbl));
	tbl->name = name;
}

int mac_table_read(struct mac_table *tbl, const char *fname)
{
	FILE *f = fopen(fname, "r");
	unsigned int num = 0;
	char line[256];

	if (!f) {
		fprintf(stderr, "Cannot open mac table %s: %s\n", fname, strerror(errno));
		return -errno;
	}

	printf("Reading MAC table %s from file %s...\n", tbl->name, fname);

	while (fgets(line, sizeof(line), f)) {
		uint8_t addr[6];
		int rc;

		if (line[strlen(line)-1] == '\n')
			line[strlen(line)-1] = 0;

		if (strlen(line) < 1)
			continue;

		/* skip comments */
		if (line[0] == ';' || line[9] == '#')
			continue;

		rc = sscanf(line, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
			    &addr[0], &addr[1], &addr[2], &addr[3], &addr[4], &addr[5]);
		if (rc != 6) {
			fprintf(stderr, "%s: Cannot parse line '%s'\n", tbl->name, line);
			continue;
		}
		printf("%s: Read MAC address %s\n", tbl->name, mac2str(addr));

		mac_table_add(tbl, addr);
		num++;
	}
	fclose(f);

	return num;
}
