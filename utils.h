#pragma once

void osmo_revbytebits_buf(uint8_t *buf, int len);

const char *mac2str(const uint8_t *addr);
const char *mac2str_buf(char *buf, const uint8_t *addr);

#define MAC_STR_SIZE	(6*3)
