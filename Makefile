
CFLAGS = -Wall -g

tr-bridge: tr-bridge.c utils.c mac_table.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	@rm -f tr-bridge
